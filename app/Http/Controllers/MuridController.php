<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Murid;
use App\Wilayah;
use App\Kategori;
use Session;
use Validator;
use Response;

class MuridController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $murid = Murid::all();
        return view('master.murid', compact('murid'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['wilayah'] = Wilayah::all();
        $data['kategori'] = Kategori::all();
        return view('master.murid-tambah', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all(); die;
        $cek = Validator::make($request->all(), [
            'nama' => ['required', 'string', 'unique:murids'],
            'tempat' => ['required', 'string'],
            'tanggal_lahir' => ['required', 'string'],
            'id_wilayah' => ['required'],
            'id_kategori' => ['required'],
        ],[
            'nama.required' => 'Nama Wajib Diisi !',
            'nama.unique' => 'Nama Sudah Ada !',
            'tempat.required' => 'Tempat Wajib Diisi !',
            'tanggal_lahir.required' => 'Tanggal Lahir Wajib Diisi !',
            'id_wilayah.required' => 'Wilayah Wajib Diisi !',
            'id_kategori.required' => 'Kategori Wajib Diisi !',
        ]);
        if ($cek->fails()) {
            Session::flash('gagal', $cek->errors());
            return redirect('murid/create');
        } else {
            $murid = New Murid();
            $murid->nama = $request['nama'];
            $murid->tempat = $request['tempat'];
            $murid->tanggal_lahir = $request['tanggal_lahir'];
            $murid->alamat = $request['alamat'];
            $murid->id_wilayah = $request['id_wilayah'];
            $murid->id_kategori = $request['id_kategori'];
            $murid->save();
            Session::flash('sukses','Berhasil Simpan !');
            return redirect('wilayah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['wilayah'] = Wilayah::all();
        $data['kategori'] = Kategori::all();
        $data['murid'] = Murid::find($id);
        // return $data; die;
        return view('master.murid-edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cek = Validator::make($request->all(), [
                'nama' => ['required', 'string', 'unique:murids,nama,'.$id],
                'tempat' => ['required', 'string'],
                'tanggal_lahir' => ['required', 'string'],
                'id_wilayah' => ['required'],
                'id_kategori' => ['required'],
            ],[
                'nama.required' => 'Nama Wajib Diisi !',
                'nama.unique' => 'Nama Sudah Ada !',
                'tempat.required' => 'Tempat Wajib Diisi !',
                'tanggal_lahir.required' => 'Tanggal Lahir Wajib Diisi !',
                'id_wilayah.required' => 'Wilayah Wajib Diisi !',
                'id_kategori.required' => 'Kategori Wajib Diisi !',
        ]);
        if ($cek->fails()) {
            Session::flash('gagal', $cek->errors());
            return redirect('murid/'.$id.'/edit');
        } else {
            $murid = Murid::where('id', $request['id'])->first();
            $murid->nama = $request['nama'];
            $murid->tempat = $request['tempat'];
            $murid->tanggal_lahir = $request['tanggal_lahir'];
            $murid->alamat = $request['alamat'];
            $murid->id_wilayah = $request['id_wilayah'];
            $murid->id_kategori = $request['id_kategori'];
            $murid->update();
            Session::flash('sukses', 'Berhasil Edit !');
            return redirect('wilayah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}