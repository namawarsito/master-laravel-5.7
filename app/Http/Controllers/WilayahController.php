<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wilayah;
use Session;
use Validator;
use Response;

class WilayahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wilayah = Wilayah::all();
        return view('master.wilayah', compact('wilayah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.wilayah-tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all(); die;
        $cek = Validator::make($request->all(), [
            'nama_wilayah' => ['required', 'string', 'unique:wilayahs'],
            'jarak' => ['required', 'string'],
            'harga' => ['required', 'string'],
        ],[
            'nama_wilayah.required' => 'Nama Wilayah Wajib Diisi !',
            'nama_wilayah.unique' => 'Nama Wilayah Sudah Ada !',
            'jarak.required' => 'Jarak Wajib Diisi !',
            'harga.required' => 'Harga Wajib Diisi !',
        ]);
        if ($cek->fails()) {
            Session::flash('gagal', $cek->errors());
            return redirect('wilayah/create');
        } else {
            $wilayah = New Wilayah();
            $wilayah->nama_wilayah = $request['nama_wilayah'];
            $wilayah->jarak = $request['jarak'];
            $wilayah->harga = $request['harga'];
            $wilayah->save();
            Session::flash('sukses','Berhasil Simpan !');
            return redirect('wilayah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $wilayah = Wilayah::find($id);
        return view('master.wilayah-edit', compact('wilayah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cek = Validator::make($request->all(), [
            'nama_wilayah' => ['required', 'string', 'unique:wilayahs,nama_wilayah,'.$id],
            'jarak' => ['required', 'string'],
            'harga' => ['required', 'string'],
        ],[
            'nama_wilayah.required' => 'Nama Wilayah Wajib Diisi !',
            'nama_wilayah.unique' => 'Nama Wilayah Sudah Ada !',
            'jarak.required' => 'Jarak Wajib Diisi !',
            'harga.required' => 'Harga Wajib Diisi !',
        ]);
        if ($cek->fails()) {
            Session::flash('gagal', $cek->errors());
            return redirect('wilayah/'.$id.'/edit');
        } else {
            $wilayah = Wilayah::where('id', $request['id'])->first();
            $wilayah->nama_wilayah = $request['nama_wilayah'];
            $wilayah->jarak = $request['jarak'];
            $wilayah->harga = $request['harga'];
            $wilayah->update();
            Session::flash('sukses', 'Berhasil Edit !');
            return redirect('wilayah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
