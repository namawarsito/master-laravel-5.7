<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Periode;
use Session;
use Validator;

class PeriodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periode = Periode::all();
        return view('master.periode', compact('periode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.periode-tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all(); die;
        $cek = Validator::make($request->all(), [
            'nama' => ['required', 'string', 'unique:periodes'],
        ],[
            'nama.required' => 'Nama Wajib Diisi !',
            'nama.unique' => 'Nama Sudah Ada !',
        ]);
        if ($cek->fails()) {
            Session::flash('gagal', $cek->errors());
            return redirect('periode/create');
        } else {
            $periode = New Periode();
            $periode->nama = $request['nama'];
            $periode->save();
            Session::flash('sukses','Berhasil Simpan !');
            return redirect('periode');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $periode = Periode::find($id);
        return view('master.periode-edit', compact('periode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cek = Validator::make($request->all(), [
            'nama' => ['required', 'string', 'unique:periodes,nama,'.$id],
        ],[
            'nama.required' => 'Nama Wajib Diisi !',
            'nama.unique' => 'Nama Sudah Ada !',
        ]);
        if ($cek->fails()) {
            Session::flash('gagal', $cek->errors());
            return redirect('periode/'.$id.'/edit');
        } else {
            $periode = Periode::where('id', $request['id'])->first();
            $periode->nama = $request['nama'];
            $periode->update();
            Session::flash('sukses', 'Berhasil Edit !');
            return redirect('periode');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
