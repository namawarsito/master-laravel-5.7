<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use Session;
use Validator;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::all();
        return view('master.kategori', compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.kategori-tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all(); die;
        $cek = Validator::make($request->all(), [
            'nama' => ['required', 'string', 'unique:kategoris'],
        ],[
            'nama.required' => 'Nama Wajib Diisi !',
            'nama.unique' => 'Nama Sudah Ada !',
        ]);
        if ($cek->fails()) {
            Session::flash('gagal', $cek->errors());
            return redirect('kategori/create');
        } else {
            $kategori = New kategori();
            $kategori->nama = $request['nama'];
            $kategori->save();
            Session::flash('sukses','Berhasil Simpan !');
            return redirect('kategori');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::find($id);
        return view('master.kategori-edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cek = Validator::make($request->all(), [
            'nama' => ['required', 'string', 'unique:kategoris,nama,'.$id],
        ],[
            'nama.required' => 'Nama Wajib Diisi !',
            'nama.unique' => 'Nama Sudah Ada !',
        ]);
        if ($cek->fails()) {
            Session::flash('gagal', $cek->errors());
            return redirect('kategori/'.$id.'/edit');
        } else {
            $kategori = Kategori::where('id', $request['id'])->first();
            $kategori->nama = $request['nama'];
            $kategori->update();
            Session::flash('sukses', 'Berhasil Edit !');
            return redirect('kategori');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
