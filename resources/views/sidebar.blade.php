<!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          @if (!empty(Auth::user()->foto))
            <img src="{{ asset('images/users/'.Auth::user()->foto) }}" class="img-circle" alt="User Image">
          @else
            <img src="{{ asset('images/users/default.jpg') }}" class="img-circle" alt="User Image">
          @endif
        </div>
        <div class="pull-left info">
          <p>{{  Auth::user()->name }}</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">PENGATURAN USER</li>
        <li class="{{ set_active(['user.index', 'user.show']) }}"><a href="{{ route('user.index') }}"><i class="fa fa-users"></i> <span>User</span></a></li>
        <li class="{{ set_active(['role.index']) }}"><a href="{{ route('role.index') }}"><i class="fa fa-signal"></i> <span>Role</span></a></li>
        <li class="header">PENGATURAN MASTER</li>
        <li class="{{ set_active(['periode.index','periode.edit','periode.create']) }}"><a href="{{ url('periode') }}"><i class="fa fa-calendar"></i> <span>Periode</span></a></li>
        <li class="{{ set_active(['kategori.index','kategori.edit','kategori.create']) }}"><a href="{{ url('kategori') }}"><i class="fa fa-list"></i> <span>kategori</span></a></li>
        <li class="{{ set_active(['wilayah.index','wilayah.edit','wilayah.create']) }}"><a href="{{ url('wilayah') }}"><i class="fa fa-map"></i> <span>Wilayah</span></a></li>
        <li class="{{ set_active(['murid.index','murid.edit','murid.create']) }}"><a href="{{ url('murid') }}"><i class="fa fa-user"></i> <span>Murid</span></a></li>


      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
