@extends('template')

@section('title')
  Wilayah <a class="btn btn-default btn-sm btn-flat" href="{{ route('wilayah.create') }}"><i class="fa fa-plus"></i> TAMBAH</a>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Wilayah</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Wilayah

        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{-- <strong>{{ $errors->has('nama') ? 'has-error' : '' }}</strong> --}}
                    </div>
                @endif
                <table class="table table-hover table-condensed table-bordered">
					<thead>
						<tr>
							<th>Nama Wilayah</th>
                            <th>Jarak</th>
                            <th>Harga</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
                        @isset($wilayah)
                            @foreach ($wilayah as $item)
                                <tr>
                                    <th>{{ $item->nama_wilayah }}</th>
                                    <th>{{ $item->jarak }}</th>
                                    <th>{{ $item->harga }}</th>
                                    <th>
                                        <a type="button" href="{{ route('wilayah.edit', $item->id) }}" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i></a>
                                    </th>
                                </tr>
                            @endforeach
                        @endisset
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">

  </script>
@endsection
