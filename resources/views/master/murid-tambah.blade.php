@extends('template')

@section('title')
  Murid
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Murid</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Murid
        	</h3>
		</div>
        <form method="POST" class="form-horizontal" action="{{ route('murid.store') }}">
            <div class="box-body">
                @if ($sukses = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{-- <strong>{{ json_encode($sukses->nama)  }}</strong> --}}
                    </div>
                @endif
                @csrf @method('POST')
                <div class="col-md-6">
                  <div class="form-group groupName">
                      <label for="nama" class="col-sm-3 control-label">Nama</label>
                      <div class="col-sm-9">
                          <input type="text" name="nama" class="form-control" value="">
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                    <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                    <div class="col-sm-9">
                        <textarea name="alamat" class="form-control" value=""></textarea>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                      <label for="tempat" class="col-sm-3 control-label">Tempat</label>
                      <div class="col-sm-9">
                          <input type="text" name="tempat" class="form-control" value="">
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                    <label for="tanggal_lahir" class="col-sm-3 control-label">Tanggal Lahir</label>
                    <div class="col-sm-9">
                        <input type="date" name="tanggal_lahir" class="form-control" value="">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                    <label class="col-sm-3 control-label">Wilayah</label>
                    <div class="col-sm-9">
                      <select name="id_wilayah" class="form-control select2">
                        <option value="">Pilih</option>
                        @foreach ($wilayah as $item)
                          <option value="{{ $item->id }}">{{ $item->nama_wilayah }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                    <label class="col-sm-3 control-label">Kategori</label>
                    <div class="col-sm-9">
                      <select name="id_kategori" class="form-control select2">
                        <option value="">Pilih</option>
                        @foreach ($kategori as $item)
                          <option value="{{ $item->id }}">{{ $item->nama }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            </div>
        </form>
	</div>
@endsection

@section('script')
  <script type="text/javascript">

  </script>
@endsection
