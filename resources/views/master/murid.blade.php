@extends('template')

@section('title')
  Murid <a class="btn btn-default btn-sm btn-flat" href="{{ route('murid.create') }}"><i class="fa fa-plus"></i> TAMBAH</a>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Murid</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Murid

        	</h3>
		</div>
		<div class="box-body">
            <div class="table-responsive">
                @if ($sukses = Session::get('sukses'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{-- <strong>{{ $errors->has('nama') ? 'has-error' : '' }}</strong> --}}
                    </div>
                @endif
                <table class="table table-hover table-condensed table-bordered" id="datatable">
					<thead>
						<tr>
							<th>Nama</th>
                            <th>Tempat, Tanggal Lahir</th>
                            <th>Alamat</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
                        @isset($murid)
                            @foreach ($murid as $item)
                                <tr>
                                    <th>{{ $item->nama }}</th>
                                    <th>{{ $item->tempat }}, {{ $item->tanggal_lahir }}</th>
                                    <th>{{ $item->alamat }}</th>
                                    <th>
                                        <a type="button" href="{{ route('murid.edit', $item->id) }}" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-edit"></i></a>
                                    </th>
                                </tr>
                            @endforeach
                        @endisset
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>

@endsection

@section('script')
  <script type="text/javascript">

  </script>
@endsection
