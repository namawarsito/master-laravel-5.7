@extends('template')

@section('title')
  Murid 
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Murid</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Murid
        	</h3>
		</div>
        <form method="POST" class="form-horizontal" action="{{ route('murid.update', $murid->id) }}">
            <div class="box-body">
                @if ($sukses = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $sukses }}</strong>
                    </div>
                @endif
                @csrf @method('PUT')
                <input type="text" name="id" value="{{ $murid->id }}" hidden>
                <div class="col-md-6">
                  <div class="form-group groupName">
                      <label for="nama" class="col-sm-3 control-label">Nama</label>
                      <div class="col-sm-9">
                          <input type="text" name="nama" class="form-control" value="{{ $murid->nama }}">
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                    <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                    <div class="col-sm-9">
                        <textarea name="alamat" class="form-control">{{ $murid->alamat }}</textarea>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                      <label for="tempat" class="col-sm-3 control-label">Tempat</label>
                      <div class="col-sm-9">
                          <input type="text" name="tempat" class="form-control" value="{{ $murid->tempat }}">
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                    <label for="tanggal_lahir" class="col-sm-3 control-label">Tanggal Lahir</label>
                    <div class="col-sm-9">
                        <input type="date" name="tanggal_lahir" class="form-control" value="{{ $murid->tanggal_lahir }}">
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                    <label class="col-sm-3 control-label">Wilayah</label>
                    <div class="col-sm-9">
                      <select name="id_wilayah" class="form-control select2" value="{{ $murid->id_wilayah }}">
                        <option value="">Pilih</option>
                        @foreach ($wilayah as $item)
                          @if ($item->id == $murid->id_wilayah)
                            <option value="{{ $item->id }}" selected>{{ $item->nama_wilayah }}</option>
                          @else
                            <option value="{{ $item->id }}">{{ $item->nama_wilayah }}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                    <label class="col-sm-3 control-label">Kategori</label>
                    <div class="col-sm-9">
                      <select name="id_kategori" class="form-control select2" value="{{ $murid->id_kategori }}">
                        <option value="">Pilih</option>
                        @foreach ($kategori as $item)
                          @if ($item->id == $murid->id_kategori)
                            <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                          @else
                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                          @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            </div>
        </form>
	</div>
@endsection

@section('script')
  <script type="text/javascript">

  </script>
@endsection
