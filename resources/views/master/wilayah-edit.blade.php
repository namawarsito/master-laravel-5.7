@extends('template')

@section('title')
  Wilayah 
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Wilayah</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-calendar"></i>
            <h3 class="box-title">
            Data Wilayah
        	</h3>
		</div>
        <form method="POST" class="form-horizontal" action="{{ route('wilayah.update', $wilayah->id) }}">
            <div class="box-body">
                @if ($sukses = Session::get('gagal'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        {{-- <strong>{{ $sukses }}</strong> --}}
                    </div>
                @endif
                @csrf @method('PUT')
                <input type="text" name="id" value="{{ $wilayah->id }}" hidden>
                <div class="col-md-6">
                  <div class="form-group groupName">
                      <label for="nama_wilayah" class="col-sm-3 control-label">Nama Walayah</label>
                      <div class="col-sm-9">
                          <input type="text" name="nama_wilayah" class="form-control" value="{{ $wilayah->nama_wilayah }}">
                          <small class="text-danger errorNamaWilayah"></small>
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                    <label for="jarak" class="col-sm-3 control-label">Jarak</label>
                    <div class="col-sm-9">
                        <input type="number" name="jarak" class="form-control" value="{{ $wilayah->jarak }}">
                        <small class="text-danger errorJarak"></small>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group groupName">
                    <label for="harga" class="col-sm-3 control-label">Harga</label>
                    <div class="col-sm-9">
                        <input type="number" name="harga" class="form-control" value="{{ $wilayah->harga }}">
                        <small class="text-danger errorHarga"></small>
                    </div>
                  </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
            </div>
        </form>
	</div>
@endsection

@section('script')
  <script type="text/javascript">

  </script>
@endsection
