@extends('template')

@section('title')
  User <button class="btn btn-default btn-sm btn-flat" onclick="addForm()"><i class="fa fa-plus"></i> TAMBAH</button>
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">User</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-users"></i>
            <h3 class="box-title">
            Data User

        	</h3>
		</div>
		<div class="box-body">
			<div class="table-responsive">
				<table class="table table-hover table-condensed table-bordered">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama</th>
							<th>Email</th>
							<th>Role</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>
{{-- MODAL USER --}}
<div class="modal fade" id="modalUser">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form method="POST" class="form-horizontal" id="formUser">
					@csrf @method('POST')
					<input type="hidden" name="id" value="">

					<div class="form-group groupName">
						<label for="name" class="col-sm-4 control-label">Nama Lengkap</label>
						<div class="col-sm-8">
							<input type="text" name="name" class="form-control" value="">
							<small class="text-danger errorName"></small>
						</div>
					</div>
					<div class="form-group groupEmail">
						<label for="email" class="col-sm-4 control-label">Alamat Email</label>
						<div class="col-sm-8">
							<input type="text" name="email" class="form-control" value="">
							<small class="text-danger errorEmail"></small>
						</div>
					</div>
					<div class="form-group groupPassword">
						<label for="password" class="col-sm-4 control-label">Password</label>
						<div class="col-sm-8">
							<input type="password" name="password" class="form-control" value="">
							<small class="text-danger errorPassword"></small>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="col-sm-4 control-label">Konfirmasi Password</label>
						<div class="col-sm-8">
							<input type="password" name="password_confirmation" class="form-control" value="">
						</div>
					</div>
					<div class="form-group groupRole">
						<label for="password" class="col-sm-4 control-label">Role User</label>
						<div class="col-sm-8">
							<div id="viewRole"></div>
							<small class="text-danger errorRole"></small>
						</div>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary btn-flat" onclick="saveForm()">Save</button>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')
  <script type="text/javascript">
	//SHOW DATA
    var table;
    table = $('.table').DataTable({
      'language': {
          'url': '/DataTables/datatable-language.json',
      },
      autoWidth: false,
      processing: true,
      serverSide: true,
      ajax: '{{ route('user-data') }}',
      columns: [
          {data: 'DT_RowIndex', searchable: false},
          {data: 'name'},
          {data: 'email'},
          {data: 'role'},
          {data: 'edit', searchable: false}
      ]
    });

    //RESET FORM
    function resetForm() {
    	$('.groupName').removeClass('has-error')
		$('.errorName').text('')
		$('.groupEmail').removeClass('has-error')
		$('.errorEmail').text('')
		$('.groupPassword').removeClass('has-error')
		$('.errorPassword').text('')
		$('.groupRole').removeClass('has-error')
		$('.errorRole').text('')
    }

    //ADD FORM
    function addForm() {
    	resetForm()
    	$('#modalUser').modal('show')
    	$('.modal-title').text('Tambah User')
    	$('#formUser')[0].reset()
    	$('input[type="checkbox"]').attr('checked', false);
    }

    //EDIT FORM
    function editForm(id) {
    	resetForm()
    	$('#modalUser').modal('show')
    	$('.modal-title').text('Edit User')
    	$.ajax({
    		url: '/user/'+id+'/edit',
    		type: 'GET',
    		dataType: 'json',
    	})
    	.done(function(data) {
    		$('input[name="id"]').val(id)
    		$('input[name="_method"]').val('PATCH')
    		$('input[name="name"]').val(data.user.name)
    		$('input[name="email"]').val(data.user.email)
    		$('input[type="checkbox"]').attr('checked', false);
    		$.each(data.role, function(i, role) {
    			 $('input[value="'+role.name+'"]').attr('checked', true)
    		});
    	});

    }

    //GET ROLE
    jQuery(document).ready(function() {
    	$.ajax({
    		url: '{{ route('getRole') }}',
    		type: 'GET',
    		dataType: 'json',
    	})
    	.done(function(data) {
    		$('#viewRole').empty();
    		$.each(data, function(index, val) {
    			$('#viewRole').append('<div class="col-sm-6"><input type="checkbox" name="role[]" class="form-radio" value="'+val.name+'"> '+val.display_name+'</div>')
    		});
    	});

    });

    //SAVE FORM
	function saveForm() {
		resetForm()

		var data = $('#formUser').serialize();
		var id = $('input[name="id"]').val();

		if (id != '') {
			url = '/user/'+id
		} else {
			url = '{{ route('user.store') }}'
		}

		$.ajax({
			url: url,
			type: 'POST',
			dataType: 'json',
			data: data,
		})
		.done(function(data) {
			if (data.errors) {
				if (data.errors.name) {
					$('.groupName').addClass('has-error')
					$('.errorName').text(data.errors.name[0])
				}
				if (data.errors.email) {
					$('.groupEmail').addClass('has-error')
					$('.errorEmail').text(data.errors.email[0])
				}
				if (data.errors.password) {
					$('.groupPassword').addClass('has-error')
					$('.errorPassword').text(data.errors.password[0])
				}
				if (data.errors.role) {
					$('.groupRole').addClass('has-error')
					$('.errorRole').text(data.errors.role[0])
				}
			}

			if (data.success == 1) {
				$('#formUser')[0].reset()
				$('#modalUser').modal('hide')
				table.ajax.reload()
				Swal({
				  position: 'middle',
				  type: 'success',
				  title: data.message,
				  showConfirmButton: false,
				  timer: 1500
				})
			}
		});

	}

  </script>
@endsection
