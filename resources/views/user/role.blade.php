@extends('template')

@section('title')
  Role / Hak Akses
@endsection

@section('breadcrumb')
  <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Role / Hak Akses</li>
@endsection

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<i class="fa fa-signal"></i>
            <h3 class="box-title">Role / Hak Akses</h3>
		</div>
		<div class="box-body">
			<div class="table-responsive">
				<table class="table table-hover table-bordered table-condensed">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Role</th>
							<th>Keterangan</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">

		</div>
	</div>
{{-- Role Modal --}}
<div class="modal fade" id="modalRole">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">
				<form id="formRole" method="POST" class="form-horizontal">
					@csrf @method('PUT')
					<input type="hidden" name="id" value="">
					<div class="form-group">
						<label for="display_name" class="col-sm-3 control-label">Nama Role</label>
						<div class="col-sm-9">
							<input type="text" name="display_name" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="description" class="col-sm-3 control-label">Keterangan</label>
						<div class="col-sm-9">
							<textarea name="description" class="form-control"></textarea>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary btn-flat" onclick="saveForm()">Save</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
  <script type="text/javascript">
	//SHOW DATA
    var table;
    table = $('.table').DataTable({
      'language': {
          'url': '/DataTables/datatable-language.json',
      },
      autoWidth: false,
      processing: true,
      serverSide: true,
      ajax: '{{ route('role-data') }}',
      columns: [
          {data: 'DT_RowIndex', searchable: false},
          {data: 'display_name'},
          {data: 'description'},
          {data: 'edit', searchable: false}
      ]
    });

    function editForm(id) {
    	$('#modalRole').modal('show');
    	$('.modal-title').text('Edit Role')
    	$.ajax({
    		url: '/role/'+id+'/edit',
    		type: 'GET',
    		dataType: 'json',
    	})
    	.done(function(data) {
    		$('input[name="id"]').val(data.id)
    		$('input[name="display_name"]').val(data.display_name)
    		$('textarea[name="description').val(data.description)
    	});

    }

    function saveForm() {
    	var data = $('#formRole').serialize();
    	var id = $('input[name="id"]').val();

    	$.ajax({
    		url: '/role/'+id,
    		type: 'POST',
    		dataType: 'json',
    		data: data,
    	})
    	.done(function(data) {
    		if (data.success == true) {
				$('#formRole')[0].reset()
				$('#modalRole').modal('hide')
				table.ajax.reload()
				Swal({
				  position: 'middle',
				  type: 'success',
				  title: data.message,
				  showConfirmButton: false,
				  timer: 1500
				})
			}
    	});

    }

  </script>
@endsection
