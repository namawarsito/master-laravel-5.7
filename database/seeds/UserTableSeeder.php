<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		//Create Administrator role
		$administratorRole = new Role();
		$administratorRole->name = 'administrator';
		$administratorRole->display_name = 'Administrator';
		$administratorRole->save();

		//Create Admin Role
		$adminRole = new Role();
		$adminRole->name = 'supervisor';
		$adminRole->display_name = 'Supervisor';
		$adminRole->save();

		//Create staff role
		$staffRole = new Role();
		$staffRole->name = 'operator';
		$staffRole->display_name = 'Operator';
		$staffRole->save();

		//Create Administrator User
		$administrator = new User();
		$administrator->name = 'Administrator';
		$administrator->email = 'administrator@mail.com';
		$administrator->password = bcrypt('123456');
		$administrator->foto = '';
		$administrator->save();
		$administrator->attachRole($administratorRole);
		$administrator->attachRole($adminRole);
		$administrator->attachRole($staffRole);

		//Create Admin User
		$admin = new User();
		$admin->name = 'Supervisor';
		$admin->email = 'supervisor@mail.com';
		$admin->password = bcrypt('123456');
		$admin->foto = '';
		$admin->save();
		$admin->attachRole($adminRole);
		$admin->attachRole($staffRole);

		//Create Staff Users
		$staff = new User();
		$staff->name = 'Operator';
		$staff->email = 'operator@mail.com';
		$staff->password = bcrypt('123456');
		$staff->foto = '';
		$staff->save();
		$staff->attachRole($staffRole);
	}
}
