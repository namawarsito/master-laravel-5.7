<?php

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
	Route::view('/', 'dashboard');
	Route::view('/home', 'dashboard')->name('home');

	Route::resource('user', 'UserController')->except(['create', 'show', 'destroy']);
	Route::get('user-data', 'UserController@datalist')->name('user-data');
	Route::get('getRole', 'UserController@getRole')->name('getRole');

	Route::resource('role', 'RoleController')->except(['create', 'store', 'show', 'destroy']);
	Route::get('role-data', 'RoleController@datalist')->name('role-data');

	Route::resource('periode', 'PeriodeController');
	Route::resource('kategori', 'KategoriController');
	Route::resource('wilayah', 'WilayahController');
	Route::resource('murid', 'MuridController');

});
